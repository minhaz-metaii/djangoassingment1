# About this repository.
It contains an assignment project where I have created a two pages website using django and after completing full project I have injected that in docker.  
I try to make the `readme.md` of this repository dinamic upon the branches. Means, individual branch shows that's feature behaviours.  

# Overview of this project
This is an responsive web app presenting a page of two contents.  
First content represents an image of full size of the window screen.  
Second content represents a set of four images and one button.  
The four images are in four corners and the button in the middle of the page (centred horizontally and vertically). Clicking the button causes the images to switch places.  

Once configured, users/developers need to define their custom event handlers, by writing some Python love.  

# Install Dependency
To run this project we need python 3.7(minimum) or higher.  
The linekd documents show how to install python for different OSs.
[Windows](https://phoenixnap.com/kb/how-to-install-python-3-windows) | 
[Linux](https://www.dummies.com/programming/python/how-to-install-python-on-a-linux-system/) | 
[Mac](https://installpython3.com/mac/)  

These linked documents are for installing any version of python. If there is any trouble of installing python3.7, then you can follow the steps given bellow.  

## Install Python for linux
This is only for linux OS as I think the installation of any python version is pretty much same for windows and mac, which can be found in those documents.  

Now, to install python3.7:  

- Get into root mode
```
sudo su
```

- Update apt-get before any work
```
apt-get update
```

- Install software-properties handler
```
apt-get install software-properties-common
```

- Add new search repository of apt
```
add-apt-repository ppa:deadsnakes/ppa
```

- Update apt-get to fetch newly added repositories
```
apt-get update
```

- Install any python version. Says, the version is 3.7
```
apt-get install python3.7
```
Note that: It will though create the python version avalable, but pip version of this installed python is not availabe for the host OS.  
You have to create virtual environment through this python to use it's pip.  

- To check if the version is installed or not type  
```
python3.7 --version
```

## Create virtual environment
We recommend to use `virtualenv` for development:  
Note: Installing and working with `virtualenv` are same for `Windows` and `Linux` as well as `Mac`.  
So I am going to present the guidance in general.  

From here, We assume that we have python 3.7 in our machine.  
- Start by installing `virtualenv` if you don't have it.
```
pip install virtualenv
```

- Check python location which will be the `virtualenv` python. In our case, it is `python 3.7`
```
which python3.7
```
Now, copy the output. Says it is `/usr/bin/python3.7`.  

- Specify a folder for this project and create virtual environment in that folder, where `venv_p37` is the instance of `virtualenv`.
```
virtualenv venv_p37 --python=/usr/bin/python3.7
```

- Now, clone the project repository.
```
git clone https://minhaz-metaii@bitbucket.org/minhaz-metaii/djangoassingment1.git
```  

## Run the project
The above processes are needed at only initalization of virtual environment and if they are done already then do only the bellow steps to run this project.  

- Go to the parent folder, which contains `venv_p37` folder of this project.  

- Now, enable the virtual environment if you don't activate it.
```
source venv_p37/bin/activate
```
Note the `(venv_p37)` in front of the prompt. This indicates that this terminal session operates for a virtual environment, named `venv_p37`.  

- Once enabled access the project folder where `readme.md` file is declared.
```
cd djangoassingment1
```

- Switch to the git branch which you want to run.
```
git checkout "branch name"
```

- Check package dependencies on the virtual environment and if not there then install.
```
pip install -r requirements.txt
```

- Go to the `assignment1` folder.
```
cd assignment1
```

- Now, run the project for the branch.
```
python manage.py runserver
```
Now, navigate to `http://127.0.0.1:8000/` . This will show the project result.  

## Terminate
- To terminate the project server hit `CTRL` + `C` in prompt.  
- To terminate the `virtualenv` environment type
```
deactivate
```
You can run full project by switching to the `feature/section2` braanch.  

# Part 2
The feature/section1 branch represents the tasks of part 2 of my assignment (Django Assignment 1).  
Here, I have initilized the django and create the django app called "index". The app returns a one page website with one section. It has one image that takes up the full size of the screen.

The task of part 2 is:  
- Create a simple Django app  
- Create an index view  
- The index view will return a one page website with one section   
- The page will have one image that takes up the full size of the screen.  

# Part 3 (Further develop on Part 2)
The feature/section3 branch represents represents the tasks of part 3 of my assignment (Django Assignment 1).  
Here, I have to create a one page site with two sections. A user should be able to simply scroll from section one to section two.  
The first section will have the full image as in Part 1 and the second section will display 4 images in 4 corners and a button in the middle of the page (centred horizontally and vertically).  
Clicking the button will cause the images to switch places.

The task of part 3 is:  
Create a one page site with two sections.  
- A user should be able to simply scroll from section one to section two.  
- The first section will have the full image as in Part 1 and the second section will display 4 images in 4 corners and a button in the middle of the page.  
- Clicking the button will cause the images to switch places.  

# Upgrade to docker
This project has been upgraded to work with docker and docker-compose.
This is the link of [How to install docker](https://docs.docker.com/engine/install/) and [How to install docker-compose](https://docs.docker.com/compose/install/).  
Let's install docker and docker-compose for linux and run the project on it.
To run the project using the docker-compose follow the steps given below:

- First of all we need docker and docker-compose installed in the host machine. To install them
```
sudo apt install docker.io
sudo apt  install docker-compose
```
Note: Here, I have used docker version 19.03.8 and docker-compose version 3.

- Switch to the git branch called 'feature/docker'.
```
git checkout feature/docker
```

- Open terminal at the `docker-compose.xml` file position and run the docker-compose command to build the project
```
sudo docker-compose up --build
```

- Done. Now you can visit the server through the host ip or container ip or default django ip. For example
```
127.0.0.1:8000
```
Note that, the port must be `8000`, otherwise you can't access the server.

- To terminate the process, use 'Ctrl + C' or open another terminal at the `docker-compose.xml` file position and type
```
sudo docker-compose down
```
