FROM ubuntu:20.04
RUN apt-get update -y
RUN apt-get install -y software-properties-common
RUN apt-get install -y python3-pip python3-dev
COPY . /opt
WORKDIR ./opt
RUN pip3 install -r requirements.txt
WORKDIR ./assignment1
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]

