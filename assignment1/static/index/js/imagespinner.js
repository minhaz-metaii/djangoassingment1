function switchImages() {
	let currentImage = null;
	let previousImageSource = null;
	let spinningImages = document.getElementsByClassName("spinImage");
	let numberOfImages = spinningImages.length;
	for (let imageIndex = 0; imageIndex < numberOfImages + 1; imageIndex++) {
		currentImage = (imageIndex === numberOfImages) ? 
			document.getElementById(spinningImages[0].id)
			:
			document.getElementById(spinningImages[imageIndex].id);
		currentImageSource = currentImage.getAttribute("src");
		if (imageIndex > 0) {
			currentImage.setAttribute("src", previousImageSource);
		}
		previousImageSource = currentImageSource;
	}
}
